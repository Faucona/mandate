from tools import *
import Vortex

def on_simulation_start(extension):

    create_input(extension, 'Transmission Shaft Speed', Vortex.Types.Type_VxReal)
    create_input(extension, 'Final Drive Ratio', Vortex.Types.Type_VxReal)
    create_input(extension, 'Wheel Radius', Vortex.Types.Type_VxReal)

    create_output(extension, 'Speed', Vortex.Types.Type_VxReal)

    extension.time_step = extension.getApplicationContext().getSimulationTimeStep()
    
def pre_step(extension):
    extension.outputs.Speed.value = (extension.inputs.Transmission_Shaft_Speed.value 
                              / extension.inputs.Final_Drive_Ratio.value
                              * extension.inputs.Wheel_Radius.value
                              * 3.6)
