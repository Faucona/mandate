"""
This module contains some basic functions for use inside various scripts
"""

from bisect import bisect_left
import math

# ------------------------------------------------------------------------------
# Field creation functions
#
def create_output(extension, name, o_type, default_value=None):
    """Create output field with optional default value, reset on every simulation run."""
    if extension.getOutput(name) is None:
        extension.addOutput(name, o_type)
    if default_value is not None:
        extension.getOutput(name).value = default_value
    return extension.getOutput(name)

def create_parameter(extension, name, p_type, default_value=None):
    """Create parameter field with optional default value set only when the field is created."""
    if extension.getParameter(name) is None:
        field = extension.addParameter(name, p_type)
        if default_value is not None:
            field.value = default_value
    return extension.getParameter(name)

def create_input(extension, name, i_type, default_value=None):
    """Create input field with optional default value set only when the field is created."""
    if extension.getInput(name) is None:
        field = extension.addInput(name, i_type)
        if default_value is not None:
            field.value = default_value
    return extension.getInput(name)


# ------------------------------------------------------------------------------
# Mathematical functions
#
def clamp(value, min_value, max_value):
    """Return a bounded value."""
    return min(max(value, min_value), max_value)


def lowpass_filter(extension, input_signal, output_signal, time_constant):
    """Apply a low-pass filter to a signal."""
    delta_time = extension.getApplicationContext().getSimulationTimeStep()
    value = (((delta_time * input_signal) + (time_constant * output_signal))
             / (delta_time + time_constant))
    return value

    
class Timer(object):
    """Counts down the specified amount of time.

    To use it, create a timer with the countdown_time and time_step values set. time_remaining 
    starts at 0, and restart() must be called to set the time_remaining to countdown_time. 

    update() is called to increment the timer, so it should be called once every step.

    Returns true while the timer is counting, and false when it reaches 0"""

    def __init__(self, countdown_time, time_step=1.0/60.0):
        self.countdown_time = countdown_time
        self.time_step = time_step
        self.time_remaining = 0.0

    def restart(self):
        self.time_remaining = self.countdown_time

    def update(self):
        self.time_remaining = max(self.time_remaining - self.time_step, 0.0)

    def __call__(self):
        return self.time_remaining > 0.0

