# Calculates values from the suspension

from tools import *
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Strut_Reference_Point = create_input(extension, 'Strut Reference Point', Vortex.Types.Type_VxReal)

    # Internal inputs

    # Parameters
    extension.Strut_Initial_Length = create_parameter(extension, 'Strut Initial Length', Vortex.Types.Type_VxReal)

    # ICD outputs

    # Internal outputs
    extension.Strut_Natural_Length = create_output(extension, 'Strut Natural Length', Vortex.Types.Type_VxReal)


    # Descriptions
    extension.Strut_Reference_Point.setDescription("Starting offset of the strut. Positive for longer")
    extension.Strut_Initial_Length.setDescription("Initial length of strut from geometry")
    extension.Strut_Natural_Length.setDescription("Natural length of strut spring constraint")


    extension.initial_length = extension.Strut_Initial_Length.value

def pre_step(extension):

    extension.Strut_Natural_Length.value = extension.initial_length + extension.Strut_Reference_Point.value
