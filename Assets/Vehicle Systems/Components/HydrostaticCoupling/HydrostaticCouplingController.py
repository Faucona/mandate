
from tools import *
import math
import Vortex

# Computes the final gear ratio to be applied to the gear ratio constraint of this component.
# See the coupling interface of this component.
# Reports output value from the coupling.

def on_simulation_start(extension):

    extension.iRatio                  = create_input(extension, "Ratio", Vortex.Types.Type_VxReal, 0.0)
    extension.iRatio_Scale            = create_input(extension, "Ratio Scale", Vortex.Types.Type_VxReal, 1.0)
    extension.iInput_Shaft_Speed      = create_input(extension, "Input Shaft Speed", Vortex.Types.Type_VxReal)
    extension.iOutput_Shaft_Speed     = create_input(extension, "Output Shaft Speed", Vortex.Types.Type_VxReal)
    extension.iMax_Coupling_Torque    = create_input(extension, "Max Coupling Torque", Vortex.Types.Type_VxReal, Vortex.VX_INFINITY)
    extension.iGear_Ratio_Torque      = create_input(extension, "Gear Ratio Torque", Vortex.Types.Type_VxReal)
    
    extension.oFinal_Ratio            = create_output(extension, "Final Ratio", Vortex.Types.Type_VxReal)
    extension.oSpeed_Ratio            = create_output(extension, "Speed Ratio", Vortex.Types.Type_VxReal)
    extension.oShaft_Speed            = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.oShaft_Torque           = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.oShaft_Power            = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    extension.iRatio.setDescription("To control the rotational speed ratio between the input shaft speed (wi) and the output shaft speed (wo)")
    extension.iRatio_Scale.setDescription("Scaling factor applied to the rotational speed ratio")
    extension.iInput_Shaft_Speed.setDescription("Speed of input shaft, from previous component")
    extension.iOutput_Shaft_Speed.setDescription("Speed of the shaft from the hinge of this component in rad/sec")
    extension.iMax_Coupling_Torque.setDescription("Sets the range in allowed torque to be applied to the coupling constraint \nas [-|'Max Coupling Torque'|, +|'Max Coupling Torque'|")
    extension.iGear_Ratio_Torque.setDescription("Output torque from the gear ratio constraint of this component")

    extension.oFinal_Ratio.setDescription("Final ratio as : Ratio*Ratio Scale")
    extension.oSpeed_Ratio.setDescription("Speed ratio as wo/wi")
    extension.oShaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.oShaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.oShaft_Power.setDescription("Net power on output shaft in W")
    
    
def pre_step(extension):

    extension.oFinal_Ratio.value = extension.iRatio.value * extension.iRatio_Scale.value

def post_step(extension):

    extension.oShaft_Speed.value = extension.iOutput_Shaft_Speed.value
    extension.oShaft_Torque.value = extension.iGear_Ratio_Torque.value
    extension.oShaft_Power.value = extension.oShaft_Speed.value * extension.oShaft_Torque.value
    
    if abs(extension.iInput_Shaft_Speed.value) > 1.e-6 :
        extension.oSpeed_Ratio.value = extension.iOutput_Shaft_Speed.value / extension.iInput_Shaft_Speed.value
    else:
        extension.oSpeed_Ratio.value = Vortex.VX_INFINITY
