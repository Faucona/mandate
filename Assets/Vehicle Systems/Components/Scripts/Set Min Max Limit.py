# This script converts the input value to minimum and maximum bounds such as { -|value|, +|value| } outputs.

from tools import *
import math
import Vortex

def on_simulation_start(extension):
    extension.Value = create_input(extension, "Value", Vortex.Types.Type_VxReal)
    extension.Min_Value = create_output(extension, "Min Value", Vortex.Types.Type_VxReal)
    extension.Max_Value = create_output(extension, "Max Value", Vortex.Types.Type_VxReal)
    
    extension.Value.setDescription("Value to be converted in min max values")
    extension.Min_Value.setDescription("Outputs -|Value|")
    extension.Max_Value.setDescription("Outputs +|Value|")

def pre_step(extension):
    extension.Min_Value.value = -abs(extension.Value.value)
    extension.Max_Value.value = +abs(extension.Value.value)
