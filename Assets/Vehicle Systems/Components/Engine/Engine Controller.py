from tools import *
import math
import csv
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Engine_Running = create_input(extension, "Engine Running", Vortex.Types.Type_Bool)
    extension.Throttle = create_input(extension, "Throttle", Vortex.Types.Type_VxReal)
    extension.Throttle_Delay = create_input(extension, "Throttle Delay", Vortex.Types.Type_VxReal)
    extension.Torque_Scale = create_input(extension, "Torque Scale", Vortex.Types.Type_VxReal)
    extension.Torque_Added = create_input(extension, "Torque Added", Vortex.Types.Type_VxReal)
    extension.Braking_Torque_Scale = create_input(extension, "Braking Torque Scale", Vortex.Types.Type_VxReal)
    extension.Braking_Torque_Added = create_input(extension, "Braking Torque Added", Vortex.Types.Type_VxReal)

    # Internal inputs
    extension.Hinge_Velocity = create_input(extension, "Hinge Velocity", Vortex.Types.Type_VxReal)
    extension.Hinge_Torque = create_input(extension, "Hinge Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Friction = create_input(extension, "Shaft Friction", Vortex.Types.Type_VxReal)

    # Parameters
    extension.Torque_Table = create_parameter(extension, "Torque Table", Vortex.Types.Type_VxFilename)
    extension.Idle_RPM = create_parameter(extension, "Idle RPM", Vortex.Types.Type_VxReal)

    # ICD outputs
    extension.Max_Torque = create_output(extension, "Max Torque", Vortex.Types.Type_VxReal)
    extension.Max_RPM = create_output(extension, "Max RPM", Vortex.Types.Type_VxReal)
    extension.Max_Power = create_output(extension, "Max Power", Vortex.Types.Type_VxReal)
    extension.Max_HP = create_output(extension, "Max HP", Vortex.Types.Type_VxReal)
    extension.RPM_at_Max_Power = create_output(extension, "RPM at Max Power", Vortex.Types.Type_VxReal)
    extension.RPM_at_Max_Torque = create_output(extension, "RPM at Max Torque", Vortex.Types.Type_VxReal)
    extension.Engine_RPM = create_output(extension, "Engine RPM", Vortex.Types.Type_VxReal)
    extension.Engine_HP = create_output(extension, "Engine HP", Vortex.Types.Type_VxReal)
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Delayed_Throttle = create_output(extension, "Delayed Throttle", Vortex.Types.Type_VxReal)
    extension.Desired_Speed = create_output(extension, "Desired Speed", Vortex.Types.Type_VxReal)
    extension.Constraint_Min_Torque = create_output(extension, "Constraint Min Torque", Vortex.Types.Type_VxReal)
    extension.Constraint_Max_Torque = create_output(extension, "Constraint Max Torque", Vortex.Types.Type_VxReal)

    extension.Engine_Running.setDescription("Set the engine to be running. When False, engine drive torque will be zero, though engine braking torque will still be applied")
    extension.Throttle.setDescription("Input throttle between 0 and 1")
    extension.Throttle_Delay.setDescription("Set the maximum rate the throttle can be increased, simulating the effect of engine response lag. Throttle rate of change will be limited so it can go from 0 to 1 in the time specified")
    extension.Torque_Scale.setDescription("Scaling factor that is multiplied by the current torque from the torque table")
    extension.Torque_Added.setDescription("Torque added to the computed engine torque")
    extension.Braking_Torque_Scale.setDescription("Scaling factor that is multiplied by the current torque from the braking torque table")
    extension.Braking_Torque_Added.setDescription("Torque added to the computed engine braking torque")
    extension.Hinge_Velocity.setDescription("Current velocity from hinge constraint")
    extension.Hinge_Torque.setDescription("Applied torque from hinge constraint")
    extension.Shaft_Friction.setDescription("Friction applied to engine hinge")
    extension.Torque_Table.setDescription("Engine torque table csv file with full throttle torque specified at each RPM")
    extension.Idle_RPM.setDescription("Idle RPM")
    extension.Max_Torque.setDescription("Maximum engine torque that can be delivered by the engine, calculated from the engine torque table")
    extension.Max_RPM.setDescription("Maximum engine RPM, calculated from the engine torque table")
    extension.Max_Power.setDescription("Maximum engine power in W that can be delivered by the engine, calculated from the engine torque table")
    extension.Max_HP.setDescription("Maximum engine power in HP that can be delivered by the engine, calculated from the engine torque table")
    extension.RPM_at_Max_Power.setDescription("Engine RPM at max power, calculated from the engine torque table")
    extension.RPM_at_Max_Torque.setDescription("Engine RPM at max torque, calculated from the engine torque table")
    extension.Engine_RPM.setDescription("Current engine RPM")
    extension.Engine_HP.setDescription("Current power applied by engine in HP")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")
    extension.Delayed_Throttle.setDescription("Throttle after applying Throttle Delay")
    extension.Desired_Speed.setDescription("Desired RPM sent to engine constraint")
    extension.Constraint_Min_Torque.setDescription("Min torque applied to engine constraint")
    extension.Constraint_Max_Torque.setDescription("Max torque applied to engine constraint")

    # Torque Table
    # Full throttle torque table is defined by user
    try:
        with open(extension.Torque_Table.value, 'r') as f:
            reader = csv.reader(f)
            your_list = list(reader)  
    except Exception as e: 
        print(e)
        extension.torque_table = None
        extension.braking_torque_table = None
        raise ValueError("Unable to load Torque Table")
            
    num_list = [[float(x) for x in rec] for rec in your_list[2:]]
    rpm = [x[0] for x in num_list]
    torque = [x[1] for x in num_list]

    # Extract basic values from torque table
    idle_rpm = extension.Idle_RPM.value
    max_torque = max(torque)
    max_rpm = rpm[-1]
    power = [x*y for x,y in zip(rpm,torque)]
    max_p = max(power)
    max_power = max_p / 30*math.pi
    max_hp = max_power * 0.00134102
    rpm_at_max_torque = rpm[torque.index(max_torque)]
    rpm_at_max_power = rpm[power.index(max_p)]

    # Add a point a 0 rpm if there is not already
    if rpm[0] > 0:
        rpm.insert(0, 0)
        torque.insert(0, 0.1 * max_torque)

    extension.Max_Torque.value = max_torque
    extension.Max_RPM.value = max_rpm
    extension.Max_Power.value = max_power
    extension.Max_HP.value = max_hp
    extension.RPM_at_Max_Power.value = rpm_at_max_power
    extension.RPM_at_Max_Torque.value = rpm_at_max_torque

    extension.torque_table = LinearInterpolation(rpm, torque)

    # Less then full throttle is derived by multiplying by a standard set of curves
    # Defined in spreadsheet
    throttle = [0.0, 0.25, 0.5, 0.75, 1.0]
    rpm =  [ 0.0,   
                idle_rpm, 
                idle_rpm + 0.1*(max_rpm - idle_rpm), 
                idle_rpm + 0.25*(max_rpm - idle_rpm), 
                idle_rpm + 0.5*(max_rpm - idle_rpm), 
                max_rpm, 
                max_rpm + 1]
    torque = [[1.0, 1.0,  1.0 , 1.0 , 1.0],
                [1.0, 1.0,  1.0 , 1.0 , 1.0],
                [0.0, 0.8,  0.96, 0.98, 1.0],
                [0.0, 0.5,  0.9 , 0.95, 1.0],
                [0.0, 0.0,  0.6 , 0.9 , 1.0],
                [0.0, 0.0,  0.0 , 0.5 , 1.0],
                [0.0, 0.0,  0.0 , 0.5 , 1.0]]

    extension.throttle_multiplier = BilinearInterpolation(throttle, rpm, torque)

    # Desired RPM curve 
    des = [idle_rpm, idle_rpm + 0.5*(max_rpm - idle_rpm), max_rpm, max_rpm, max_rpm]
    extension.desired_rpm = LinearInterpolation(throttle, des)

        # Brake torque table linearly increases to max torque at max rpm
    extension.braking_torque_table = LinearInterpolation([-1.0, 0.0, max_rpm], [0.0, 0.0, -max_torque])



def pre_step(extension):

    if (extension.torque_table is None) or (extension.desired_rpm is None) or (extension.braking_torque_table is None):
        return

    extension.throttle_rate = extension.getApplicationContext().getSimulationTimeStep() / max(extension.Throttle_Delay.value, 0.001)
    extension.Delayed_Throttle.value = min(extension.Throttle.value, extension.Delayed_Throttle.value + extension.throttle_rate)
    throttle = extension.Delayed_Throttle.value
    rpm = extension.Hinge_Velocity.value * 30 / math.pi


    if extension.inputs.Engine_Running.value:
        extension.Desired_Speed.value = extension.desired_rpm(throttle) / 30 * math.pi
        torque_raw = extension.torque_table(rpm) * extension.throttle_multiplier(throttle, rpm)
        extension.Constraint_Max_Torque.value = max(torque_raw * extension.Torque_Scale.value 
                                                + extension.Torque_Added.value, 0)
        # print "rpm: {}, torq: {}, mult: {}".format(rpm, extension.torque_table(rpm), extension.throttle_multiplier(throttle, rpm))
    else:
        extension.Desired_Speed.value = 0
        extension.Constraint_Max_Torque.value = 0

    braking_torque_raw = extension.braking_torque_table(max(rpm - extension.Desired_Speed.value * 30 / math.pi, 0))
    extension.Constraint_Min_Torque.value = min(braking_torque_raw * extension.Braking_Torque_Scale.value 
                                            + extension.Braking_Torque_Added.value, 0)

    extension.Engine_RPM.value = rpm
    extension.Shaft_Speed.value = extension.Hinge_Velocity.value
    # Field reports net torque, so subtract friction torque from output torque
    # Since friction could be + or -, must use abs for subtraction, then reapply sign.
    extension.Shaft_Torque.value = math.copysign(max(abs(extension.Hinge_Torque.value) - extension.Shaft_Friction.value, 0), extension.Hinge_Torque.value)
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value
    extension.Engine_HP.value = extension.Shaft_Power.value * 0.00134102
