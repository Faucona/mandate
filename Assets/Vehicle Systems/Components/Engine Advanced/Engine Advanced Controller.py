from tools import *
import math
import csv
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Engine_Running = create_input(extension, "Engine Running", Vortex.Types.Type_Bool)
    extension.Throttle = create_input(extension, "Throttle", Vortex.Types.Type_VxReal)
    extension.Throttle_Delay = create_input(extension, "Throttle Delay", Vortex.Types.Type_VxReal)
    extension.Torque_Scale = create_input(extension, "Torque Scale", Vortex.Types.Type_VxReal)
    extension.Torque_Added = create_input(extension, "Torque Added", Vortex.Types.Type_VxReal)
    extension.Braking_Torque_Scale = create_input(extension, "Braking Torque Scale", Vortex.Types.Type_VxReal)
    extension.Braking_Torque_Added = create_input(extension, "Braking Torque Added", Vortex.Types.Type_VxReal)

    # Internal inputs
    extension.Engine_Speed = create_input(extension, "Engine Speed", Vortex.Types.Type_VxReal)

    # Parameters
    extension.Torque_Table = create_parameter(extension, "Torque Table", Vortex.Types.Type_VxFilename)
    extension.Braking_Torque_Table = create_parameter(extension, "Braking Torque Table", Vortex.Types.Type_VxFilename)
    extension.Desired_RPM = create_parameter(extension, "Desired RPM", Vortex.Types.Type_VxFilename)

    # ICD outputs
    extension.Max_Torque = create_output(extension, "Max Torque", Vortex.Types.Type_VxReal)
    extension.Max_RPM = create_output(extension, "Max RPM", Vortex.Types.Type_VxReal)
    extension.Max_Power = create_output(extension, "Max Power", Vortex.Types.Type_VxReal)
    extension.Max_HP = create_output(extension, "Max HP", Vortex.Types.Type_VxReal)
    extension.Idle_RPM = create_output(extension, "Idle RPM", Vortex.Types.Type_VxReal)
    extension.RPM_at_Max_Power = create_output(extension, "RPM at Max Power", Vortex.Types.Type_VxReal)
    extension.RPM_at_Max_Torque = create_output(extension, "RPM at Max Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_RPM = create_output(extension, "Shaft RPM", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Delayed_Throttle = create_output(extension, "Delayed Throttle", Vortex.Types.Type_VxReal)
    extension.Desired_Speed = create_output(extension, "Desired Speed", Vortex.Types.Type_VxReal)
    extension.Constraint_Min_Torque = create_output(extension, "Constraint Min Torque", Vortex.Types.Type_VxReal)
    extension.Constraint_Max_Torque = create_output(extension, "Constraint Max Torque", Vortex.Types.Type_VxReal)

    extension.Engine_Running.setDescription("True when engine is running, false when off")
    extension.Throttle.setDescription("Input throttle between 0 and 1")
    extension.Throttle_Delay.setDescription("Set the limits of the rate of increase of the throttle input")
    extension.Torque_Scale.setDescription("Scaling factor that is multiplied by the current torque from the braking torque table")
    extension.Torque_Added.setDescription("Torque added to the computed engine braking torque")
    extension.Braking_Torque_Scale.setDescription("Scaling factor that is multiplied by the current torque from the torque table")
    extension.Braking_Torque_Added.setDescription("Torque added to the computed engine  torque")
    extension.Engine_Speed.setDescription("Speed from engine shaft")
    extension.Torque_Table.setDescription("Engine torque table csv file")
    extension.Braking_Torque_Table.setDescription("Engine brake torque table csv file")
    extension.Desired_RPM.setDescription("Table showing desired RPM at each throttle position. If left empty, this will be derived automatically from the torque table")
    extension.Max_Torque.setDescription("Maximum engine torque that can be delivered by the engine, calculated from the engine torque table")
    extension.Max_RPM.setDescription("Maximum engine RPM, calculated from the engine torque table")
    extension.Max_Power.setDescription("Maximum engine power in watt that can be delivered by the engine, calculated from the engine torque table")
    extension.Max_HP.setDescription("Maximum engine power in HP that can be delivered by the engine, calculated from the engine torque table")
    extension.Idle_RPM.setDescription("Engine idle RPM according to the torque table, calculated from the engine torque table")
    extension.RPM_at_Max_Power.setDescription("Engine RPM at max power, calculated from the engine torque table")
    extension.RPM_at_Max_Torque.setDescription("Engine RPM at max Torque, calculated from the engine torque table")
    extension.Shaft_RPM.setDescription("Current engine RPM")
    extension.Shaft_Torque.setDescription("Current torque from the torque table")
    extension.Delayed_Throttle.setDescription("Throttle after applying Throttle Delay")
    extension.Desired_Speed.setDescription("Desired RPM sent to engine constraint")
    extension.Constraint_Min_Torque.setDescription("Min torque sent to engine constraint")
    extension.Constraint_Max_Torque.setDescription("Max torque sent to engine constraint")

    # Torque Table
    try:
        with open(extension.Torque_Table.value, 'r') as f:
            reader = csv.reader(f)
            your_list = list(reader)  
            
    except:
        extension.torque_table = None
        raise ValueError("Unable to load Torque Table")

    num_list = [[float(x) for x in rec] for rec in your_list[2:]]
    throttle = num_list[0][-1:0:-1] # reverse since table has reversed throttle order
    rpm = [x[0] for x in num_list][1:]
    torque = [row[-1:0:-1] for row in num_list][1:]

    extension.torque_table = BilinearInterpolation(throttle, rpm, torque)

    # Extract basic values from torque table
    torque_transp = [list(x) for x in zip(*torque)]

    full_torque = torque_transp[-1]
    max_torque = max(full_torque)
    max_rpm = rpm[-1]
    power = [x*y for x,y in zip(rpm,full_torque)]
    max_p = max(power)
    max_power = max_p / 30*math.pi
    max_hp = max_power * 0.00134102
    idle_rpm = rpm[torque_transp[0].index(0)]
    rpm_at_max_torque = rpm[full_torque.index(max_torque)]
    rpm_at_max_power = rpm[power.index(max_p)]

    extension.Max_Torque.value = max_torque
    extension.Max_RPM.value = max_rpm
    extension.Max_Power.value = max_power
    extension.Max_HP.value = max_hp
    extension.Idle_RPM.value = idle_rpm
    extension.RPM_at_Max_Power.value = rpm_at_max_power
    extension.RPM_at_Max_Torque.value = rpm_at_max_torque



    # If specified, use an explicitly defined desired RPM table
    if extension.Desired_RPM.value:
        try:
            with open(extension.Desired_RPM.value, 'r') as f:
                reader = csv.reader(f)
                your_list = list(reader)  
        except:
            extension.desired_rpm = None
            raise ValueError("Unable to load Desired RPM table")
                
        des_num_list = [[float(x) for x in rec] for rec in your_list]
        des_throttle = des_num_list[0]
        des_rpm = des_num_list[1]
        extension.desired_rpm = LinearInterpolation(des_throttle, des_rpm)

    # Otherwise Extract desired RPM from torque table: Use RPM where there is 0 torque
    else:
        des = []
        for i, tor in enumerate(torque_transp):
            # Find the RPM where torque is 0
            if 0 in tor:
                des.append(rpm[tor.index(0)])
            # If it doesn't reach zero, use max RPM
            else:
                des.append(rpm[-1])
        extension.desired_rpm = LinearInterpolation(throttle, des)

    # Braking Torque Table
    try:
        with open(extension.Braking_Torque_Table.value, 'r') as f:
            reader = csv.reader(f)
            your_list = list(reader)  
    except:
        extension.braking_torque_table = None
        raise ValueError("Unable to load Braking Torque Table")
        
    brake_num_list = [[float(x) for x in rec] for rec in your_list[2:]]
    brake_throttle = brake_num_list[0][-1:0:-1] # reverse since table has reversed throttle order
    brake_rpm = [x[0] for x in brake_num_list][1:]
    brake_torque = [row[-1:0:-1] for row in brake_num_list][1:]

    extension.braking_torque_table = BilinearInterpolation(brake_throttle, brake_rpm, brake_torque)
       

def pre_step(extension):

    if (extension.torque_table is None) or (extension.desired_rpm is None) or (extension.braking_torque_table is None):
        return

    extension.throttle_rate = extension.getApplicationContext().getSimulationTimeStep() / max(extension.Throttle_Delay.value, 0.001)
    extension.Delayed_Throttle.value = min(extension.Throttle.value, extension.Delayed_Throttle.value + extension.throttle_rate)
    throttle = extension.Delayed_Throttle.value
    rpm = extension.Engine_Speed.value * 60 / 2 / math.pi


    extension.Shaft_RPM.value = rpm

    if extension.inputs.Engine_Running.value:
        extension.Desired_Speed.value = extension.desired_rpm(throttle) / 60 * 2 * math.pi
        torque_raw = extension.torque_table(throttle, rpm)
        print (torque_raw * extension.Torque_Scale.value + extension.Torque_Added.value)
        extension.Constraint_Max_Torque.value = max(torque_raw * extension.Torque_Scale.value 
                                                + extension.Torque_Added.value, 0)
    else:
        extension.Desired_Speed.value = 0
        extension.Constraint_Max_Torque.value = 0

    braking_torque_raw = extension.braking_torque_table(throttle, max(rpm-extension.Desired_Speed.value, 0.0))
    extension.Constraint_Min_Torque.value = min(braking_torque_raw * extension.Braking_Torque_Scale.value 
                                            + extension.Braking_Torque_Added.value, 0)

    extension.Shaft_Torque.value = extension.Constraint_Max_Torque.value
