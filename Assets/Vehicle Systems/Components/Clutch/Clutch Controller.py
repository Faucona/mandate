# This script could eventually become a full torque converter model, but currently it is a basic automatic clutch that 
# engages linearly between the Engage Min RPM and Engage Max RPM. 

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Engage_Min_RPM = create_input(extension, 'Engage Min RPM', Vortex.Types.Type_VxReal)
    extension.Engage_Max_RPM = create_input(extension, 'Engage Max RPM', Vortex.Types.Type_VxReal)
    extension.Max_Torque = create_input(extension, 'Max Torque', Vortex.Types.Type_VxReal)

    # Internal inputs
    extension.Input_Shaft_Speed = create_input(extension, "Input Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Output_Shaft_Speed = create_input(extension, "Output Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Gear_Ratio_Torque = create_input(extension, "Gear Ratio Torque", Vortex.Types.Type_VxReal)

    # ICD outputs
    extension.Coupling_Torque = create_output(extension, "Coupling Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Coupling_Torque_Positive = create_output(extension, "Coupling Torque Positive", Vortex.Types.Type_VxReal)
    extension.Coupling_Torque_Negative = create_output(extension, "Coupling Torque Negative", Vortex.Types.Type_VxReal)



    extension.Engage_Min_RPM.setDescription("RPM to start engaging clutch")
    extension.Engage_Max_RPM.setDescription("RPM where clutch is fully engaged and max torque is applied")
    extension.Max_Torque.setDescription("Max torque to apply at Engine Max RPM")
    extension.Input_Shaft_Speed.setDescription("Speed of input shaft, from previous component")
    extension.Output_Shaft_Speed.setDescription("Speed of the output shaft from the hinge of this component")
    extension.Gear_Ratio_Torque.setDescription("Torque on gear ratio constraint of this component")
    extension.Coupling_Torque.setDescription("Torque applied to coupling constraint")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")
    extension.Coupling_Torque_Positive.setDescription("Positive of torque to be applied to coupling constraint")
    extension.Coupling_Torque_Negative.setDescription("Negative of torque to be applied to coupling constraint")


    # Linear interpolation between 0 and Max Torque as RPM goes from Engage Min to Engage Max
    extension.engage = LinearInterpolation([0, extension.inputs.Engage_Min_RPM.value, extension.inputs.Engage_Max_RPM.value, 100000], 
                                         [0, 0, extension.inputs.Max_Torque.value, extension.inputs.Max_Torque.value])

def pre_step(extension):

    coupling = extension.engage(extension.inputs.Input_Shaft_Speed.value)


    extension.Coupling_Torque.value = - extension.Gear_Ratio_Torque.value
    extension.Shaft_Speed.value = extension.Output_Shaft_Speed.value
    extension.Shaft_Torque.value = extension.Coupling_Torque.value
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value

    extension.Coupling_Torque_Positive.value = coupling
    extension.Coupling_Torque_Negative.value = - coupling
