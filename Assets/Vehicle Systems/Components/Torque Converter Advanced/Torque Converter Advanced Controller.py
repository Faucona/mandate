# This script models a hydrodynamic torque converter, based on the original Vehicle Systems C++ implementation

from tools import *
import math
import csv
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Coupling_Torque_Scale = create_input(extension, "Coupling Torque Scale", Vortex.Types.Type_VxReal, 1)
    extension.Coupling_Torque_Added = create_input(extension, "Coupling Torque Added", Vortex.Types.Type_VxReal, 0)
    extension.Stall_RPM = create_input(extension, "Stall RPM", Vortex.Types.Type_VxReal, 1000)
    extension.Stall_Torque = create_input(extension, "Stall Torque", Vortex.Types.Type_VxReal, 100)
    extension.Torque_Mutiplication = create_input(extension, "Torque Mutiplication", Vortex.Types.Type_VxReal, 1)

    # Internal inputs
    extension.Input_RPM = create_input(extension, "Input RPM", Vortex.Types.Type_VxReal)
    extension.Output_Shaft_Speed = create_input(extension, "Output Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Last_Coupling_Torque = create_input(extension, "Last Coupling Torque", Vortex.Types.Type_VxReal)

    # Parameters
    extension.Lambda_Table = create_parameter(extension, "Lambda Table", Vortex.Types.Type_VxFilename)

    # ICD outputs
    extension.Speed_Ratio = create_output(extension, "Speed Ratio", Vortex.Types.Type_VxReal)
    extension.Shaft_RPM = create_output(extension, "Shaft RPM", Vortex.Types.Type_VxReal)
    extension.Coupling_Torque = create_output(extension, "Coupling Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Coupling_Torque_Negative = create_output(extension, "Coupling Torque Negative", Vortex.Types.Type_VxReal)
    extension.Multiplication_Torque = create_output(extension, "Multiplication Torque", Vortex.Types.Type_VxReal)
    extension.Multiplication_Desired_RPM = create_output(extension, "Multiplication Desired RPM", Vortex.Types.Type_VxReal)


    extension.Coupling_Torque_Scale.setDescription("Multiplies the coupling torque calculated from the torque converter model")
    extension.Coupling_Torque_Added.setDescription("Torque that is directly added to the coupling torque. This can be used to simulate a locking mechanism. A negative torque can be used to reduce the overall coupling")
    extension.Stall_RPM.setDescription("Stall RPM")
    extension.Stall_Torque.setDescription("Stall torque")
    extension.Torque_Mutiplication.setDescription("Torque multiplication factor at 0 speed ratio")
    extension.Input_RPM.setDescription("RPM of input shaft, from previous component")
    extension.Output_Shaft_Speed.setDescription("Speed of the output shaft from the hinge of this component")
    extension.Lambda_Table.setDescription("Torque converter lambda table filename in csv format. If blank, default table will be used")
    extension.Speed_Ratio.setDescription("Speed ratio, output shaft / input shaft")
    extension.Shaft_RPM.setDescription("Speed of output shaft in RPM")
    extension.Coupling_Torque.setDescription("Torque applied to coupling constraint")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")
    extension.Coupling_Torque_Negative.setDescription("Negative of torque to be applied to coupling constraint")
    extension.Multiplication_Torque.setDescription("Torque to be applied to hinge constraint to simulate torque multiplication")
    extension.Multiplication_Desired_RPM.setDescription("Desired RPM of hinge constraint to simulate torque multiplication")


    # Lambda Table
    # If specified, use an explicitly defined table
    if extension.Lambda_Table.value:
        try:
            with open(extension.Lambda_Table.value, 'r') as f:
                reader = csv.reader(f)
                your_list = list(reader)  
        except:
            extension.lambda_table = None
            raise ValueError("Unable to load Lambda Table")
            
        num_list = [[float(x) for x in rec] for rec in your_list[2:]]
        csr = [x[0] for x in num_list]
        mult = [x[1] for x in num_list]
        extension.lambda_table = LinearInterpolation(csr, mult)

    else:
        extension.lambda_table = None
        # Generate a lambda curve from a polynomial, since it's smoother than interpolated points
        extension.lambda_coefficients = [-2.7604, 3.4129, -1.9984, 0.371, 0.978]
        extension.lambda_overspeed_table = LinearInterpolation([0, 1, 2], [0, 0, 1])
        # extension.lambda_table = LinearInterpolation(csr, mult)
        

    # Previous lambda value for filtering
    extension.lamb_prev = calculate_lambda(extension, 0)

def paused_update(extension):
    pass
#     extension.outputs.Coupling_Torque.value = 0.001
#     extension.outputs.Coupling_Torque_Negative.value = -0.001

def pre_step(extension):

    # Calculate drag coefficient that corresponds to stall RPM and torque
    if extension.Stall_Torque.value <= 0 or extension.Stall_RPM.value <= 0 or calculate_lambda(extension, 0) <= 0:
        extension.drag_coeff = 0
        raise ValueError("Stall Torque or Stall RPM invalid")
    else:        
        extension.drag_coeff = extension.Stall_Torque.value / (extension.Stall_RPM.value * math.pi/30.0)**2 / calculate_lambda(extension, 0)
        print(extension.drag_coeff)

    # Torque multiplier is max at 0 speed ratio, reduces to 0 at 0.8 speed ratio
    extension.mult_table = LinearInterpolation([0, 0.8, 1], 
                                         [extension.Torque_Mutiplication.value, 0, 0])

    # Calculate input shaft speed in rad/s, avoid negative rotation or infinite value when used in denominator
    input_speed = max(extension.Input_RPM.value *math.pi/30.0, 0.0001)
    # Calculate speed ratio, cap at 10 since it's meaningless when high anyway
    sr = min(max(extension.Output_Shaft_Speed.value / input_speed, 0), 10.0)
    # Calculate lambda
    lamb = calculate_lambda(extension, sr)

    extension.lamb_prev = lowpass_filter(extension, lamb, extension.lamb_prev, 0.0)

    # Calculate coupling force from standard TC model, plus lock torque
    coupling = max(input_speed**2 * extension.drag_coeff * extension.Coupling_Torque_Scale.value * extension.lamb_prev 
                + extension.Coupling_Torque_Added.value, 0.001)

    if  coupling < abs(extension.Last_Coupling_Torque.value) + 10000.0: # effectively disabled by adding 10000
        extension.Coupling_Torque.value = coupling
    else:
        # Equivalent time constant from old torque converter was 0.221
        extension.Coupling_Torque.value = lowpass_filter(extension, coupling, abs(extension.Last_Coupling_Torque.value), 0.2)

    mult_torque = extension.Coupling_Torque.value * max(extension.mult_table(sr) - 1, 0)


    extension.Speed_Ratio.value = sr
    extension.Shaft_RPM.value = extension.Output_Shaft_Speed.value *30.0/math.pi
    extension.Coupling_Torque_Negative.value = -extension.Coupling_Torque.value

    extension.Multiplication_Torque.value = mult_torque
    extension.Multiplication_Desired_RPM.value = input_speed

    extension.Shaft_Speed.value = extension.Output_Shaft_Speed.value
    extension.Shaft_Torque.value = extension.Coupling_Torque.value + extension.Multiplication_Torque.value
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value

def calculate_lambda(extension, sr):
    if (extension.lambda_table is None):
        if sr <= 1:
            return polynomial(sr, extension.lambda_coefficients)
        else:
            return extension.lambda_overspeed_table(sr)
    else:
        return extension.lambda_table(sr)

def polynomial(input, coefficients):
    res = 0
    order = len(coefficients)
    for i, a in enumerate(coefficients):
        # print "a: {:6.2f}, exp: {}".format(a, order-i-1)
        res += a*input**(order-i-1)
    return res
